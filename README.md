# tokio-msgpack-rpc

An implementation of the [msgpack-RPC] protocol in Rust, using the [tokio] and
[rmp] libraries.

This library is unstable and subject to backwards-incompatible changes without
warning.

## Getting Started

Read the documentation or look at the [examples](examples) directory.

## Acknowledgments

The `msgpack_rpc` macro API is inspired by the `service` macro from [tarpc].

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or
   http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

[msgpack-RPC]: https://github.com/msgpack-rpc/msgpack-rpc/blob/master/spec.md
[tokio]: https://github.com/tokio-rs/tokio
[rmp]: https://github.com/3Hren/msgpack-rust
[tarpc]: https://github.com/google/tarpc
