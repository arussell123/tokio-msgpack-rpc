extern crate futures;
extern crate porthole;
extern crate tokio_core;
extern crate rmpv as msgpack;

use std::thread;
use std::net::ToSocketAddrs;

use tokio_core::reactor::Core;

#[macro_use]
extern crate tokio_msgpack_rpc as msgpack_rpc;

use msgpack_rpc::Client;

mod arithmetic {
    msgpack_rpc! {
        rpc add(x: i64, y: i64) -> i64 | ();
        rpc subtract(x: i64, y: i64) -> i64 | ();
        rpc multiply(x: i64, y: i64) -> i64 | ();
        rpc divide(x: i64, y: i64) -> i64 | ();
    }
}

#[derive(Default)]
struct Arithmetic;

impl arithmetic::Service for Arithmetic {
    fn add(&self, x: i64, y: i64) -> Result<i64, ()> {
        Ok(x + y)
    }

    fn subtract(&self, x: i64, y: i64) -> Result<i64, ()> {
        Ok(x - y)
    }

    fn multiply(&self, x: i64, y: i64) -> Result<i64, ()> {
        Ok(x * y)
    }

    fn divide(&self, x: i64, y: i64) -> Result<i64, ()> {
        Ok(x / y)
    }
}

#[test]
fn multiple_methods() {
    use msgpack::Value;

    let mut core = Core::new().unwrap();

    let addr = ("localhost", porthole::open().unwrap())
        .to_socket_addrs()
        .unwrap()
        .into_iter()
        .next()
        .unwrap();

    thread::spawn(move || arithmetic::Server::listen::<Arithmetic>(addr));

    let handle = core.handle();

    let client = core.run(Client::connect(&addr, &handle)).unwrap();

    let response = core.run(call_async!(client, "add", 1, 1)).unwrap();
    assert_eq!(response, Ok(Value::from(2)));

    let response = core.run(call_async!(client, "subtract", 9, 3)).unwrap();
    assert_eq!(response, Ok(Value::from(6)));

    //                call_async!(client, "subtract", 9, 3)
    //            })
    //            .and_then(|response| {
    //                let expected = Ok(Value::I64(6));
    //                assert_eq!(response, expected);
    //
    //                call_async!(client, "multiply", 9, 9)
    //            })
    //            .and_then(|response| {
    //                let expected = Ok(Value::I64(81));
    //                assert_eq!(response, expected);
    //
    //                call_async!(client, "divide", 7, 2)
    //            })
    //            .and_then(|response| {
    //                let expected = Ok(Value::I64(3));
    //                assert_eq!(response, expected);
    //
    //                Ok(())
    //            })
    // }));
}
