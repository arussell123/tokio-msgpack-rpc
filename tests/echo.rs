#[macro_use]
extern crate tokio_msgpack_rpc as msgpack_rpc;

extern crate futures;
extern crate rmpv as msgpack;
extern crate rmp_serde as msgpack_serde;
extern crate tokio_core;
extern crate porthole;

use std::net::ToSocketAddrs;

use msgpack::Value;
use msgpack_rpc::Client;
use tokio_core::reactor::Core;

mod echo {
    msgpack_rpc! {
        rpc echo(arg: Value) -> Value | ();
    }
}

#[derive(Default)]
struct Echo;

impl echo::Service for Echo {
    fn echo(&self, arg: Value) -> Result<Value, ()> {
        return Ok(arg);
    }
}

#[test]
fn simple() {
    use std::thread;

    let mut core = Core::new().unwrap();

    let addr = ("localhost", porthole::open().unwrap())
        .to_socket_addrs()
        .unwrap()
        .into_iter()
        .next()
        .unwrap();

    thread::spawn(move || echo::Server::listen::<Echo>(addr));

    let handle = core.handle();

    let client = core.run(Client::connect(&addr, &handle)).unwrap();

    let response = core.run(call_async!(client, "echo", "Hello, world!")).unwrap();
    assert_eq!(response, Ok(Value::String("Hello, world!".into())));
}

#[test]
fn multiple_types() {
    use std::thread;

    let mut core = Core::new().unwrap();

    let addr = ("localhost", porthole::open().unwrap())
        .to_socket_addrs()
        .unwrap()
        .into_iter()
        .next()
        .unwrap();

    thread::spawn(move || echo::Server::listen::<Echo>(addr));

    let handle = core.handle();

    let client = core.run(Client::connect(&addr, &handle)).unwrap();

    let response = core.run(call_async!(client, "echo", "Hello, world!")).unwrap();
    assert_eq!(response, Ok(Value::String("Hello, world!".into())));

    let response = core.run(call_async!(client, "echo", vec![1, 2, 3])).unwrap();
    assert_eq!(response, Ok(Value::Array(vec![Value::from(1), Value::from(2), Value::from(3)])));
}

#[test]
fn client() {
    use std::thread;

    let mut core = Core::new().unwrap();

    let addr = ("localhost", porthole::open().unwrap())
        .to_socket_addrs()
        .unwrap()
        .into_iter()
        .next()
        .unwrap();

    thread::spawn(move || echo::Server::listen::<Echo>(addr));

    let client = echo::Client::connect(&addr, &mut core);

    let response = core.run(client.echo(Value::String("Hello, world!".into()))).unwrap();
    assert_eq!(response, Ok(Value::String("Hello, world!".into())));
}
