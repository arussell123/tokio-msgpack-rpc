//! Provides utilities for parsing and serializing msgpack-RPC responses

use std::fmt;

use msgpack::Value;
use serde;
use serde::de::{Error, Unexpected};

const RESPONSE_TYPE: u32 = 1;

/// A msgpack-RPC response.
#[derive(Debug, Clone, PartialEq)]
pub struct Response {
    id: u32,
    result: Result<Value, Value>,
}

impl Response {
    /// Creates a new `Response` object with an empty error and the given result.
    pub fn success(id: u32, result: Value) -> Response {
        Response {
            id: id,
            result: Ok(result),
        }
    }

    /// Creates a new `Response` object with an empty result and the given error.
    pub fn error(id: u32, error: Value) -> Response {
        Response {
            id: id,
            result: Err(error),
        }
    }

    /// Consumes the `Response` object and converts it into a `Result` containing the `Response`'s
    /// result and error.
    pub fn into_result(self) -> Result<Value, Value> {
        self.result
    }

    /// Returns the ID of the `Request` that generated this response.
    pub fn id(&self) -> u32 {
        self.id
    }
}

impl serde::Serialize for Response {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: serde::Serializer
    {
        let (result, error) = match self.result {
            Ok(ref result) => (result.to_owned(), Value::Nil),
            Err(ref error) => (Value::Nil, error.to_owned()),
        };

        let response = (1_u32, self.id, error, result);

        response.serialize(serializer)
    }
}

impl serde::Deserialize for Response {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer
    {
        struct ResponseVisitor;

        impl serde::de::Visitor for ResponseVisitor {
            type Value = Response;

            fn visit_seq<V>(self, mut visitor: V) -> Result<Self::Value, V::Error>
                where V: serde::de::SeqVisitor
            {
                let msg_type: u32 = try!(try!(visitor.visit())
                    .ok_or(V::Error::missing_field("type")));

                if msg_type != RESPONSE_TYPE {
                    return Err(V::Error::invalid_value(Unexpected::Unsigned(msg_type as u64), &"response marker"));
                }

                let id = try!(try!(visitor.visit()).ok_or(V::Error::missing_field("id")));

                let error = try!(try!(visitor.visit::<Value>())
                    .ok_or(V::Error::missing_field("error")));

                let result = try!(try!(visitor.visit::<Value>())
                    .ok_or(V::Error::missing_field("result")));

                Ok(Response {
                    id: id,
                    result: if error == Value::Nil {
                        Ok(result)
                    } else {
                        Err(error)
                    },
                })
            }

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                write!(formatter, "a valid msgpack-RPC response")
            }
        }

        deserializer.deserialize(ResponseVisitor)
    }
}
