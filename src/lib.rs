//! An implementation of the [msgpack-RPC] protocol in Rust.
//!
//! # Getting Started
//!
//! See the documentation for the [`msgpack_rpc!`](macro.msgpack_rpc!.html) macro or the `examples`
//! directory.
//!
//! # Acknowledgments
//!
//! This implementation is built on the [tokio] and [rmp] projects.
//!
//! [msgpack-RPC]: https://github.com/msgpack-rpc/msgpack-rpc/blob/master/spec.md
//! [tokio]: https://github.com/tokio-rs/tokio
//! [rmp]: https://github.com/3Hren/msgpack-rust

#![warn(missing_docs)]

#[macro_use]
extern crate log;

pub extern crate futures;
pub extern crate tokio_core;
pub extern crate tokio_service;

extern crate bytes;
extern crate serde;
extern crate tokio_io;
extern crate tokio_proto;

pub extern crate rmpv as msgpack;
pub extern crate rmp_serde as msgpack_serde;

pub mod client;
pub mod server;

pub use client::Client;
pub use server::Server;
pub use request::{Request, Type};
pub use response::Response;

/// The error type expected by the RPC service.
pub type Error = std::io::Error;

/// The Future type returned by RPC servers.
pub type Future = Box<futures::Future<Item = Response, Error = Error>>;

mod request;
mod response;

/// A macro for declaring a new msgpack-RPC service.
///
/// This macro generates structs and traits to assist in creating and using an RPC service.
///
/// # Overview
///
/// ## Defining a service
///
/// Use the `msgpack_rpc!` macro to define your service. The syntax is similar to a function
/// declaration. Consider declaring a service that sends messages to users. On success, our server
/// returns nothing (`()`). If the service fails, it sends back an error message. We could declare
/// our service like so:
///
/// ```
/// # #[macro_use] extern crate tokio_msgpack_rpc;
/// msgpack_rpc! {
///     rpc send_message(username: String, message: String) -> () | String;
/// }
/// # fn main() {}
/// ```
///
/// Let's parse this declaration. `rpc` serves as an indicator that we are declaring a remote
/// procedure call signature. Next, we see that the function takes two arguments, `username` and
/// `message`, of type `&str`. The return type looks slightly different from a normal Rust return
/// type. The return types indicate which types are returned from the server on success or error.
/// In this case, the server returns `()` on success, and `String` on error.
///
/// > *Note*: Argument and return types *must* have an implementation of `Into<rmp_serde::Value>`
/// > and `serde::Deserialize` for the service declaration to compile. Most primitive types already
/// > have an implementation of both. If an implementation does not exist for the type you wish to
/// > use with the service, you can always declare your service to use `rmp_serde::Value`
/// > directly, and manually convert types as needed.
///
/// ## Using the macro
///
/// If your service is declared correctly, the macro will expand into a number of traits and
/// structs used to interact with your service.
///
/// - `trait Service`
///
///     Implement this trait to provide the implementation of your server. The trait looks very
///     similar to the RPC definition, with a few differences:
///
///     1. The trait methods are provided an immutable reference to `self`. This allows servers to
///        have interior mutability.
///     2. The return type is a `Result` of the success and error types from the definition.
///
///     ```no_run
///     # #[macro_use] extern crate tokio_msgpack_rpc;
///     #
///     # msgpack_rpc! {
///     #     rpc send_message(username: String, message: String) -> () | String;
///     # }
///     #[derive(Default)]
///     struct Message;
///
///     impl Service for Message {
///         fn send_message(&self, username: String, message: String) -> Result<(), String> {
///             // ... Code to send a message ...
///             Ok(())
///         }
///     }
///
///     # fn main() {}
///     ```
///
/// - `struct Server`
///
///     A server that is ready to serve an implementation of your service.
///
///     ```no_run
///     # #[macro_use] extern crate tokio_msgpack_rpc;
///     #
///     # msgpack_rpc! {
///     #     rpc send_message(username: String, message: String) -> () | String;
///     # }
///     #
///     # #[derive(Default)]
///     # struct Message;
///     #
///     # impl Service for Message {
///     #     fn send_message(&self, username: String, message: String) -> Result<(), String> {
///     #         // ... Code to send a message ...
///     #         Ok(())
///     #     }
///     # }
///     #
///     fn main() {
///         Server::listen::<Message>("127.0.0.1:0".parse().unwrap());
///     }
///     ```
///
/// # Examples
///
/// ```no_run
/// #[macro_use]
/// extern crate tokio_msgpack_rpc as msgpack_rpc;
///
/// extern crate futures;
/// extern crate rmpv as msgpack;
/// extern crate tokio_core;
///
/// use std::thread;
///
/// use msgpack::Value;
/// use msgpack_rpc::Client;
/// use tokio_core::reactor::Core;
///
/// // The macro creates items that may clash with existing items,
/// // so we expand it inside a module.
/// mod sum {
///     msgpack_rpc! {
///         rpc sum(x: i32, y: i32) -> i32 | ();
///     }
/// }
///
/// // Implement the service.
/// #[derive(Default)]
/// struct Sum;
///
/// impl sum::Service for Sum {
///     fn sum(&self, x: i32, y: i32) -> Result<i32, ()> {
///         Ok(x + y)
///     }
/// }
///
/// # fn main() {
/// // To use the service:
/// let mut core = Core::new().unwrap();
/// let handle = core.handle();
///
/// let addr = "127.0.0.1:0".parse().unwrap();
/// thread::spawn(move || sum::Server::listen::<Sum>(addr));
///
/// // To just run the server forever:
/// // core.run(futures::empty::<(), ()>()).unwrap();
///
/// // Otherwise, start a client and connect to the server.
/// let client = core.run(Client::connect(&addr, &handle)).unwrap();
///
/// let result = core.run(call_async!(client, "sum", 1, 1)).unwrap();
/// assert_eq!(result, Ok(Value::from(2)));
/// # }
/// ```
#[macro_export]
macro_rules! msgpack_rpc {
    (
        $(
            rpc $name:ident ( $( $arg:ident : $arg_ty:ty ),* ) -> $ret_ty:ty | $err_ty:ty;
        )+
    ) => (
        use std::io;
        use std::net::SocketAddr;

        use $crate::msgpack::Value;
        use $crate::futures::Future;
        use $crate::tokio_core::reactor::{Core, Handle};

        pub trait Service {
            $(
                #[allow(unused_variables)]
                fn $name ( &self, $( $arg : $arg_ty ),* ) -> Result<$ret_ty, $err_ty>;
            )+
        }

        #[allow(dead_code)]
        struct ServiceWrapper<S>(S);

        #[allow(unused_variables)]
        impl<S> $crate::tokio_service::Service for ServiceWrapper<S> where S: Service {
            type Request = $crate::Request;
            type Response = $crate::Response;
            type Error = io::Error;
            type Future = Box<Future<Item = Self::Response, Error = Self::Error>>;

            fn call(&self, msg: Self::Request) -> Self::Future {
                let id = msg.id().unwrap();

                let result: Result<Value, Value> = match msg.method() {
                    $(
                        stringify!($name) => {
                            let mut params = msg.params().iter();
                            let mut params = params.by_ref().cloned();

                            (self.0).$name($({
                                // Unused variable to control expansion
                                let $arg = ();

                                $crate::msgpack::ext::from_value(params.next().unwrap()).unwrap()
                            }),*)
                                .map(|res| $crate::msgpack::ext::to_value(res).unwrap())
                                .map_err(|err| $crate::msgpack::ext::to_value(err).unwrap())
                        }
                    ),+,
                    _ => Err(Value::String("method not supported".into())),
                };

                let response = match result {
                    Ok(val) => $crate::Response::success(id, val),
                    Err(err) => $crate::Response::error(id, err),
                };

                Box::new($crate::futures::done(Ok(response)))
            }
        }

        #[allow(dead_code)]
        pub struct Client {
            inner: $crate::client::Client,
            handle: Handle,
        }

        #[allow(dead_code)]
        impl Client {
            pub fn connect(addr: &SocketAddr, core: &mut Core) -> Self {
                let handle = core.handle();

                let client = core.run($crate::Client::connect(addr, &handle)).unwrap();

                Client {
                    inner: client,
                    handle: handle,
                }
            }

            $(
                 pub fn $name ( &self, $( $arg : $arg_ty ),* )
                     -> Box<Future<Item = Result<Value, Value>, Error = ::std::io::Error>> {
                     call_async!(self.inner,
                                 stringify!($name),
                                 $( msgpack::Value::from($arg) ),*)
                 }
            )+
        }

        #[allow(dead_code)]
        pub struct Server;

        #[allow(dead_code)]
        impl Server {
            pub fn listen<S>(addr: SocketAddr) where S: Service + Default {
                $crate::Server::listen(addr, || Ok(ServiceWrapper(S::default())))
            }
        }
    )
}

/// Convenience macro for sending requests to a msgpack-RPC server.
///
/// Also see [`ClientHandle::request`](./client/struct.ClientHandle.html#method.request).
///
/// # Examples
///
/// ```no_run
/// #[macro_use]
/// extern crate tokio_msgpack_rpc as msgpack_rpc;
///
/// extern crate rmpv as msgpack;
/// extern crate tokio_core;
///
/// use msgpack::Value;
/// use msgpack_rpc::Client;
/// use tokio_core::reactor::Core;
///
/// # fn main() {
/// let mut core = Core::new().unwrap();
/// let handle = core.handle();
///
/// let client = core.run(Client::connect(&"127.0.0.1:0".parse().unwrap(), &handle)).unwrap();
///
/// let result = core.run(call_async!(client, "sum", 1, 2, 3));
/// assert_eq!(result.unwrap(), Ok(Value::from(6)));
///
/// let result = core.run(call_async!(client, "sum", 2, 2));
/// assert_eq!(result.unwrap(), Ok(Value::from(4)));
/// # }
/// ```
#[macro_export]
macro_rules! call_async {
    ( $client:expr, $method:expr, $( $arg:expr ),* ) => ({
        use $crate::msgpack::{self, Value};

        let mut params: Vec<Value> = vec![];

        $(
            {
                params.push(msgpack::ext::to_value($arg).unwrap());
            }
        )*

        ($client).request(($method), params)
    });

    ( $client:expr, $method:expr) => ( 
        ($client).request(($method), vec![])
    )
}

#[cfg(test)]
mod test {
    #[test]
    fn macro_smoke() {
        msgpack_rpc! {
            rpc echo(arg: Value) -> Value | ();
        }
    }
}
