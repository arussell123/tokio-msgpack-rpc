//! Provides utilities for parsing and serializing msgpack-RPC requests.

use std::fmt;

use msgpack::Value;
use serde::{self, Serialize};
use serde::ser::Error as SerializeError;
use serde::de::Error;
use serde::de::Unexpected;

const REQUEST_TYPE: u32 = 0;
const NOTIFICATION_TYPE: u32 = 2;

#[derive(Debug, Copy, Clone)]
/// The type of a [`Request`](./struct.Request.html).
pub enum Type {
    /// An RPC request. Expects a response from the server.
    Request,

    /// An RPC notification. Does not expect a response from the server.
    Notification,
}

/// An RPC request.
///
/// A Request can either be of type `Request` or `Notification`. The only functional difference is
/// that the client does not expect a response for a notification.
#[derive(Debug, Clone)]
pub struct Request {
    /// The ID of the Request.
    ///
    /// Messages with the Request type must have IDs.
    ///
    /// Notifications do not have IDs.
    ///
    /// As an implementation detail, it's possible
    /// that the `Request` struct does not have an ID because the transport has not yet assigned
    /// one. However, it is an error to attempt to deserialize a Request without an ID.
    id: Option<u32>,

    method: String,
    params: Vec<Value>,

    msg_type: Type,
}

impl Request {
    /// Creates a new `Request` containing a msgpack-RPC request.
    pub fn request<M, P>(method: M, params: P) -> Request
        where M: Into<String>,
              P: IntoIterator<Item = Value>
    {
        Request {
            id: None,
            method: method.into(),
            params: params.into_iter().collect(),
            msg_type: Type::Request,
        }
    }

    /// Creates a new `Request` containing a msgpack-RPC notification.
    pub fn notification<M, P>(method: M, params: P) -> Request
        where M: Into<String>,
              P: IntoIterator<Item = Value>
    {
        Request {
            id: None,
            method: method.into(),
            params: params.into_iter().collect(),
            msg_type: Type::Notification,
        }
    }

    /// Returns the ID of the request.
    ///
    /// If the request is a `Notification`, the request does not have an ID.
    pub fn id(&self) -> Option<u32> {
        self.id
    }

    /// Assigns the request an ID.
    pub fn set_id(&mut self, id: u32) {
        self.id = Some(id);
    }

    /// Returns the name of the RPC method called by the request.
    pub fn method(&self) -> &str {
        &self.method
    }

    /// Returns the parameters of the RPC method supplied by the request.
    pub fn params(&self) -> &[Value] {
        &self.params[..]
    }

    /// Returns whether the request is a Request or Notification.
    pub fn msg_type(&self) -> Type {
        self.msg_type
    }
}

impl serde::Deserialize for Request {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer
    {
        struct RequestVisitor;

        impl serde::de::Visitor for RequestVisitor {
            type Value = Request;

            fn visit_seq<V>(self, mut visitor: V) -> Result<Self::Value, V::Error>
                where V: serde::de::SeqVisitor
            {
                let msg_type: u32 = try!(try!(visitor.visit())
                    .ok_or(V::Error::missing_field("type")));

                if msg_type != REQUEST_TYPE && msg_type != NOTIFICATION_TYPE {
                    return Err(V::Error::invalid_value(Unexpected::Unsigned(msg_type as u64), &"request or notification marker"));
                };

                let id = if msg_type == REQUEST_TYPE {
                    // Deserialize the ID of the request.
                    Some(try!(try!(visitor.visit()).ok_or(V::Error::missing_field("id"))))
                } else {
                    None
                };

                let method = try!(try!(visitor.visit()).ok_or(V::Error::missing_field("method")));

                let params = try!(try!(visitor.visit::<Vec<Value>>())
                        .ok_or(V::Error::missing_field("params")))
                    .into_iter()
                    .collect();

                let request = Request {
                    id: id,
                    method: method,
                    params: params,
                    msg_type: if id.is_some() {
                        Type::Request
                    } else {
                        Type::Notification
                    },
                };

                Ok(request)
            }

            fn expecting(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
                write!(formatter, "a valid msgpack-RPC request")
            }
        }

        deserializer.deserialize(RequestVisitor)
    }
}

impl Serialize for Request {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: serde::Serializer
    {
        match self.msg_type {
            Type::Request => {
                let id = self.id.ok_or(S::Error::custom("cannot serialize request without id"))?;
                (REQUEST_TYPE, id, &self.method, &self.params).serialize(serializer)
            }
            Type::Notification => {
                (NOTIFICATION_TYPE, &self.method, &self.params).serialize(serializer)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Request;

    use msgpack::Value;
    use msgpack_serde::Serializer;
    use serde::Serialize;

    #[test]
    fn request_must_have_id() {
        let mut buf = vec![];
        let request = Request::request("test", vec![Value::Nil]);
        let mut serializer = Serializer::new(&mut buf);

        assert!(request.serialize(&mut serializer).is_err());
    }
}
