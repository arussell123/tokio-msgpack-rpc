//! Contains client-related functionality.

use std::error::Error;
use std::io::{self, Cursor, ErrorKind};
use std::net::SocketAddr;

use bytes::{BytesMut, BufMut};
use futures::Future;
use msgpack::Value;
use msgpack_serde::decode::Error as MsgpackError;
use msgpack_serde::{Serializer, Deserializer};
use serde::{Deserialize, Serialize};
use tokio_core::net::TcpStream;
use tokio_core::reactor::Handle;
use tokio_io::codec::{Encoder, Decoder, Framed};
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_proto::TcpClient;
use tokio_proto::multiplex::ClientService;
use tokio_proto::multiplex::{ClientProto, RequestId};
use tokio_service::Service;

use request::Request;
use response::Response;

struct ClientCodec;

impl Decoder for ClientCodec {
    type Item = (RequestId, Response);
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<(RequestId, Response)>> {
        if buf.len() == 0 {
            return Ok(None);
        }

        let (response, position) = {
            let cursor = Cursor::new(&buf);
            let mut deserializer = Deserializer::new(cursor);

            match Response::deserialize(&mut deserializer) {
                Ok(response) => (response, deserializer.into_inner().position()),
                Err(MsgpackError::InvalidDataRead(..)) => return Ok(None),
                Err(err) => return Err(io::Error::new(io::ErrorKind::Other, err.description())),
            }
        };

        buf.split_to(position as usize);

        Ok(Some((response.id() as u64, response)))
    }
}

impl Encoder for ClientCodec {
    type Item = (RequestId, Request);
    type Error = io::Error;

    fn encode(&mut self, (id, mut req): (RequestId, Request), buf: &mut BytesMut) -> io::Result<()> {
        // Assign the request the ID assigned by tokio
        req.set_id(id as u32);

        let mut serializer = Serializer::new(vec![]);

        req.serialize(&mut serializer).map_err(|err| io::Error::new(ErrorKind::Other, err))?;

        buf.put_slice(serializer.get_ref());

        Ok(())
    }
}

struct RpcProto;

impl<T: AsyncRead + AsyncWrite + 'static> ClientProto<T> for RpcProto {
    type Request = Request;
    type Response = Response;
    type Transport = Framed<T, ClientCodec>;
    type BindTransport = Result<Self::Transport, io::Error>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        Ok(io.framed(ClientCodec))
    }
}

/// Handles connecting to existing msgpack-RPC services.
pub struct Client {
    inner: ClientService<TcpStream, RpcProto>,
}

impl Client {
    /// Establish a connection to an RPC server at the provided address.
    pub fn connect(addr: &SocketAddr,
                   handle: &Handle)
                   -> Box<Future<Item = Client, Error = io::Error>> {
        let future = TcpClient::new(RpcProto)
            .connect(addr, handle)
            .map(|service| Client { inner: service });

        Box::new(future)
    }

    /// Send a msgpack-rpc request to the server.
    ///
    /// Returns a future that will resolve to the response.
    pub fn request<M, P>(&self,
                         method: M,
                         params: P)
                         -> Box<Future<Item = Result<Value, Value>, Error = io::Error>>
        where M: Into<String>,
              P: IntoIterator<Item = Value>
    {
        let request = Request::request(method, params);
        Box::new(self.inner.call(request).map(|response| response.into_result()))
    }

    /// Sends a msgpack-rpc notification to the server.
    pub fn notify<M, P>(&self, method: M, params: P)
        where M: Into<String>,
              P: IntoIterator<Item = Value>
    {
        let notification = Request::notification(method, params);
        self.inner.call(notification);
    }
}
