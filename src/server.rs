//! Contains server-related functionality.
//!
//! In most cases, you will not need to create a `Server` directly. Instead, you can use the
//! [`msgpack_rpc!` macro].
//!
//! However, there are some cases where the macro is insufficient, such as if your server needs
//! to support varargs. By defining a `Service` directly, you may respond to a `Request` in any
//! way you like.
//!
//! # Examples
//!
//! If you wanted to implement a service that sums all of its arguments:
//!
//! ```no_run
//! extern crate futures;
//! extern crate rmpv as msgpack;
//! extern crate tokio_core;
//! extern crate tokio_msgpack_rpc as msgpack_rpc;
//! extern crate tokio_service;
//!
//! use futures::{Async, Future};
//! use msgpack::Value;
//! use msgpack_rpc::Server;
//! use tokio_core::reactor::Core;
//! use tokio_service::Service;
//!
//! #[derive(Debug, Clone)]
//! struct Sum;
//!
//! impl Service for Sum {
//!     type Request = msgpack_rpc::Request;
//!     type Response = msgpack_rpc::Response;
//!     type Error = msgpack_rpc::Error;
//!     type Future = msgpack_rpc::Future;
//!
//!     fn call(&self, req: Self::Request) -> Self::Future {
//!         let sum: i64 = req.params().iter().map(|p| p.as_i64().unwrap()).sum();
//!
//!         let response = msgpack_rpc::Response::success(req.id().unwrap(), Value::from(sum));
//!
//!         futures::finished(response).boxed()
//!     }
//! }
//!
//! fn main() {
//!     let mut core = Core::new().unwrap();
//!
//!     Server::listen("127.0.0.1:0".parse().unwrap(), || Ok(Sum));
//!
//!     // Listen forever
//!     core.run(futures::empty::<(), ()>()).unwrap();
//! }
//! ```
//!
//! [`msgpack_rpc!` macro]: ../macro.msgpack_rpc!.html

use std::error::Error;
use std::io::{self, Cursor, ErrorKind};
use std::net::SocketAddr;

use bytes::{BytesMut, BufMut};
use msgpack_serde::decode::Error as MsgpackError;
use msgpack_serde::{Serializer, Deserializer};
use serde::{Serialize, Deserialize};
use tokio_io::codec::{Encoder, Decoder, Framed};
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_proto::TcpServer;
use tokio_proto::multiplex::{ServerProto, RequestId};
use tokio_service::NewService;

use request::Request;
use response::Response;

struct ServerCodec;

impl Decoder for ServerCodec {
    type Item = (RequestId, Request);
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<(RequestId, Request)>> {
        if buf.len() == 0 {
            return Ok(None);
        }

        let (request, position) = {
            let cursor = Cursor::new(&buf);
            let mut deserializer = Deserializer::new(cursor);

            match Request::deserialize(&mut deserializer) {
                Ok(request) => (request, deserializer.into_inner().position()),
                Err(MsgpackError::InvalidDataRead(..)) => return Ok(None),
                Err(err) => return Err(io::Error::new(io::ErrorKind::Other, err.description())),
            }
        };

        buf.split_to(position as usize);

        Ok(Some((request.id().unwrap() as u64, request)))
    }
}

impl Encoder for ServerCodec {
    type Item = (RequestId, Response);
    type Error = io::Error;

    fn encode(&mut self, (_id, res): (RequestId, Response), buf: &mut BytesMut) -> io::Result<()> {
        let mut serializer = Serializer::new(vec![]);
        res.serialize(&mut serializer).map_err(|err| io::Error::new(ErrorKind::Other, err))?;

        buf.put_slice(serializer.get_ref());

        Ok(())
    }
}

struct RpcProto;

impl<T: AsyncRead + AsyncWrite + 'static> ServerProto<T> for RpcProto {
    type Request = Request;
    type Response = Response;

    type Transport = Framed<T, ServerCodec>;
    type BindTransport = Result<Self::Transport, io::Error>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        Ok(io.framed(ServerCodec))
    }
}

/// A server that can serve msgpack-RPC requests.
pub struct Server<T> {
    _inner: T,
}

impl<T> Server<T> where T: NewService<Request = Request, Response = Response, Error = io::Error> + Send + Sync + 'static {
    /// Starts a msgpack-RPC service on a given socket address, given a `Service` that transforms
    /// `Request`s into `Response`s.
    pub fn listen(addr: SocketAddr, new_service: T) {
        TcpServer::new(RpcProto, addr).serve(new_service)
    }
}

#[cfg(test)]
mod tests {
    use super::ServerCodec;

    use tokio_io::codec::Decoder;

    #[test]
    fn decode_short() {
        let mut buf = vec![0xd9, 0x5].into();

        assert!(ServerCodec.decode(&mut buf).unwrap().is_none());
    }
}
